static zx_protocol_device_t null_device_ops = {
    .version = DEVICE_OPS_VERSION,
    .read = null_read,
    .write = null_write,
};

static zx_driver_ops_t demo_null_driver_ops = {
    .version = DRIVER_OPS_VERSION,
    .bind = null_bind,
};
                                                  // Max size to accept         Actual size sent
static zx_status_t null_read(void* ctx, void* buff, size_t count, zx_off_t off, size_t* actual)
{
    // Indiciates EOF
    *actual = 0;
    return ZX_OK;
}

static zx_status_t null_write(void* ctx, const void* buff, size_t count, zx_off_t off, size_t* actual)
{
    *actual = count;
    return ZX_OK;
}

// Inits device
zx_status_t null_bind(void* ctx, zx_device_t* parent)
{
    device_add_args_t args = {
        .version = DEVICE_ADD_ARGS_VERSION,
        // Parent name
        .name = "demo-null",
        // Pointer to supported operations
        .ops = &null_device_ops,
    };

    // Publishes device
    // Device is bound to parent with name in args
    return device_add(parent, &args, NULL);
}

// Contains pointer to function table
ZIRCON_DRIVER_BEGIN(demo_null_driver, demo_null_driver_ops, 
    "zircon", "0.1", 1)

// If device has MISC_PARENT protocol, bind
BI_MATCH_IF(EQ, BIND_PROTOCOL, ZX_PROTOCOL_MISC_PARENT)

ZIRCON_DRIVER_END(demo_null_driver)